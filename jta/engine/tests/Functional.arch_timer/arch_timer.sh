
tarball=dung-3.4.25-m2.tar.gz

function test_build  {
    touch test_suit_ready
}

function test_deploy {
    put -r ./* $OSV_HOME/osv.$TESTDIR/
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR/arch_timer; ./arch_timer-interrupt-lager.sh ; ./dmesg-lager.sh;  ./proc-interrupts-lager.sh"
}

function test_processing {
    assert_define FUNCTIONAL_ARCH_TIMER_RES_LINES_COUNT

    check_capability "RENESAS"

    log_compare "$TESTDIR" $FUNCTIONAL_ARCH_TIMER_RES_LINES_COUNT "Test passed" "p"
}

. ../scripts/functional.sh

