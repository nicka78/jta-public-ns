{
    "testName": "Benchmark.bc",
    "fail_case": [
        {
            "fail_regexp": "some test regexp",
            "fail_message": "some test message"
        },
        {
            "fail_regexp": "Bug",
            "fail_message": "Bug or Oops detected in system log",
            "use_syslog": "1"
        }
        ],
    "specs": 
    [
        {
            "name":"bc-exp1",
            "EXPR1":"2*2",
            "EXPR2":"3*3"
        },
        {
            "name":"bc-exp2",
            "EXPR1":"2+2",
            "EXPR2":"3+3"
        },
        {
            "name":"default",
            "EXPR1":"2+2",
            "EXPR2":"3+3"
        }
    ]
}

